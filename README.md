# ProdFix: Give the hammers back to the people

## Description

This mod fixes a bug that can lead to negative production in fiefs:
![Production bug picture](https://i.imgur.com/JmTSAtv.png "Production bug")

In a nutshell, everytime the game tries to calculate the daily production for a settlement, it factors in hammer production for every buildings.

Matter is, some buildings do not produce hammers. This causes the game to add -1 production to the daily value for each of these buildings, which can pile up fast considering most do not produce hammers.

I solved it by adding a tiny check to verify the building actually produces hammers before altering the daily production.

The overriden class is `BuildingConstructionModel`.

## Compiling from source

Edit the `GameFolder` property in the project to point to your Bannerlord installation and build the solution. The mod will be mirrored to your Module folder when it is done.

## Credits & License

Many thanks to Calsev for documenting M&B Bannerlord modding with his [SmithForever](https://github.com/calsev/bannerlord_smith_forever) example mod on which I based the Visual Studio project.

This mod is licensed under the [MIT license](ProdFix/LICENSE.txt).