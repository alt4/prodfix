﻿using System;
using System.Collections.Generic;
using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.SandBox.GameComponents;
using TaleWorlds.Core;
using TaleWorlds.MountAndBlade;

namespace ProdFix
{
	public class ProdFixSubModule : MBSubModuleBase
	{
		protected override void OnGameStart(Game game, IGameStarter gameStarterObject)
		{
			if (!(game.GameType is Campaign))
			{
				return;
			}
			AddModels(gameStarterObject);
		}

		protected virtual void AddModels(IGameStarter gameStarterObject)
		{
			ReplaceModel<DefaultBuildingConstructionModel, ProdFixModel>(gameStarterObject);
		}

		protected void ReplaceModel<TBaseType, TChildType>(IGameStarter gameStarterObject)
			where TBaseType : GameModel
			where TChildType : TBaseType
		{
			if (!(gameStarterObject.Models is IList<GameModel> models))
			{
				return;
			}

			bool found = false;
			for (int index = 0; index < models.Count; ++index)
			{
				if (models[index] is TBaseType)
				{
					found = true;
					if (!(models[index] is TChildType))
					{
						models[index] = Activator.CreateInstance<TChildType>();
					}
				}
			}

			if (!found)
			{
				gameStarterObject.AddModel(Activator.CreateInstance<TChildType>());
			}
		}
	}
}
