﻿using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.SandBox.GameComponents;
using TaleWorlds.Core;
using Helpers;
using TaleWorlds.Localization;
using System.Linq;

namespace ProdFix
{
	public class ProdFixModel : DefaultBuildingConstructionModel
	{
		public override int CalculateDailyConstructionPower(Town town, StatExplainer explanation = null)
		{
			return this.CalculateDailyConstructionPowerInternal(town, explanation);
		}

		public override int CalculateDailyConstructionPowerWithoutBoost(Town town, StatExplainer explanation = null)
		{
			return this.CalculateDailyConstructionPowerWithoutBoostInternal(town, explanation);
		}

		private int CalculateDailyConstructionPowerInternal(Town town, StatExplainer explanation)
		{
			float num = town.Prosperity * 0.013f;
			if (town.BoostBuildingProcess >= 500)
			{
				num *= 2f;
			}
			return this.CalculateBuildingConstructionPowerEffects(town, explanation, num);
		}

		private int CalculateDailyConstructionPowerWithoutBoostInternal(Town town, StatExplainer explanation)
		{
			float currentLabor = town.Prosperity * 0.013f;
			return this.CalculateBuildingConstructionPowerEffects(town, explanation, currentLabor);
		}

		private int CalculateBuildingConstructionPowerEffects(Town town, StatExplainer explanation, float currentLabor)
		{
			if (town.Governor != null)
			{
				Settlement currentSettlement = town.Governor.CurrentSettlement;
				if (((currentSettlement != null) ? currentSettlement.Town : null) == town)
				{
					ExplainedNumber explainedNumber = new ExplainedNumber(0f, null);
					ExplainedNumber explainedNumber2 = new ExplainedNumber(1f, null);
					SkillHelper.AddSkillBonusForTown(DefaultSkills.Engineering, DefaultSkillEffects.TownProjectBuildingBonus, town, ref explainedNumber);
					if (town.Settlement.OwnerClan.Leader.CharacterObject.GetFeatValue(DefaultFeats.Cultural.EmpireConstructionExpert))
					{
						PerkHelper.AddFeatBonusForPerson(DefaultFeats.Cultural.EmpireConstructionExpert, town.Settlement.OwnerClan.Leader.CharacterObject, ref explainedNumber2);
					}
					currentLabor += town.Prosperity * 0.013f * (explainedNumber.ResultNumber / 100f) * explainedNumber2.ResultNumber;
					if (explainedNumber.ResultNumber > 0f && explanation != null)
					{
						explanation.AddLine(new TextObject("{=obiKXPPW}Governor Bonus", null).ToString(), explainedNumber.ResultNumber, StatExplainer.OperationType.Add);
					}
					if (explainedNumber2.ResultNumber > 1f && explanation != null)
					{
						explanation.AddLine(new TextObject("{=Eaq23Rez}Feat Bonus", null).ToString(), explainedNumber2.ResultNumber, StatExplainer.OperationType.Add);
					}
				}
			}
			ExplainedNumber explainedNumber3 = new ExplainedNumber(currentLabor, explanation, null);
			SettlementLoyaltyModel settlementLoyaltyModel = Campaign.Current.Models.SettlementLoyaltyModel;
			foreach (Building building in town.Buildings)
			{
				float num = (float)building.GetBuildingEffectAmount(DefaultBuildingEffects.HammerProduction);
				explainedNumber3.AddFactor(num / 100f, building.Name);
				num = (float)building.GetBuildingEffectAmount(DefaultBuildingEffects.Hammer);
				if (num > 0)
				{
					if (town.Loyalty >= (float)settlementLoyaltyModel.HighLoyaltyThresholdForBuildingConstructionBonus)
					{
						num += (float)settlementLoyaltyModel.HighLoyaltyBuildingConstructionEffect;
					}
					else if (town.Loyalty <= (float)settlementLoyaltyModel.LowLoyaltyThresholdForBuildingConstructionBonus)
					{
						num += (float)settlementLoyaltyModel.LowLoyaltyBuildingConstructionEffect;
					}
					explainedNumber3.Add(num, null);
				}
			}
			int num2 = town.SoldItems.Sum(delegate (Town.SellLog x)
			{
				if (x.Category.Properties != ItemCategory.Property.BonusToProduction)
				{
					return 0;
				}
				return x.Number;
			});
			if (num2 > 0)
			{
				explainedNumber3.Add(0.2f * (float)num2, ProdFixModel._productionFromMarketText);
			}
			return (int)explainedNumber3.ResultNumber;
		}

		private const int BoostSpeed = 2;

		private const float HammerMultiplier = 0.013f;

		private static readonly TextObject _productionFromMarketText = new TextObject("{=vaZDJGMx}Construction From Market", null);
	}
}
